<?php

use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Marshaler;
use GuzzleHttp\Client;

require_once 'vendor/autoload.php';


putenv("AWS_ACCESS_KEY_ID="); // replace with your own AWS_ACCESS_KEY_ID
putenv("AWS_SECRET_ACCESS_KEY="); // replace with your own AWS_SECRET_ACCESS_KEY

$storeID = 17; // only size is on launch app atm
$options = [
    'region'            => 'eu-west-1',
    'version'           => 'latest',
];
$dynamo_table_name = "adyen-event-notifications";
$client = new DynamoDbClient($options);
$marshaller = new Marshaler();

$mesh_apikey = ""; // replace with your own api key
$mesh_url = sprintf("https://prod.jdgroupmesh.cloud/stores/size/payments/adyenNotifications?api_key=%s&debug=1", $mesh_apikey);

// get payment list
if (!file_exists("payments.txt")) {
    echo "cannot locate payments.txt file";
    exit;
}
$str = file_get_contents("payments.txt");
$data = explode("\n", $str);
$paymentIDs = $data;
if (empty($paymentIDs) ) {
    echo "failed to read content of txt file properly";
    exit;
}

// loop through the list
$dynamoParams = [
    'TableName' => $dynamo_table_name,
];

$found_items = 0;
$processed_events = 0;
foreach ($paymentIDs as $key => $paymentID) {
    // paymentID is same as merchant ref in dynamo
    // echo "retrieving adyen notification event from dynamo for store ID {$storeID} using merchant reference " . $paymentID . " \n";
    $key = $marshaller->marshalItem( buildKey($paymentID, $storeID) );
    $dynamoParams['Key'] = $key;
    $response = $client->getItem($dynamoParams);

    if (!$response['Item']) {
        continue;
    }


    $item = $marshaller->unmarshalItem($response['Item']);
    $found_items ++;
    echo "found notification event for " .  $item['merchantReference'] . " \n";
    
    // get the notification event for the payment from dynamo
    // consruct the adyen notification payload to send to platform
    $notification = buildNotificationEventRequest( $item['notificationEvent'] );
    
    // actually send to platform
    $mesh_response = sendNotificationToMesh($mesh_url, $notification);

    // check that the response back from platform is 200 & [accepted]
    if ($mesh_response->getStatusCode() != 200) {
        echo "Send adyen notification to mesh failed to get a non 200 response, code {$mesh_response->code} returned \n";
        continue;
    }
    $mesh_response_body = (string) $mesh_response->getBody();
    if ($mesh_response_body != "[accepted]") {
        echo "Send adyen notification to mesh failed to get an [accepted] response, body {$mesh_response_body} returned \n";
        continue;
    }
    $processed_events++;
}

// finally print out number of processed payments
print_r(sprintf("Found %d notifications from dynamo, %d were successfully processed by mesh", $found_items, $processed_events));


function buildKey(string $merchantRef, int $storeID) : array {
    $key = [];
    $key['merchantReference'] = $merchantRef;
    // $key['storeID'] = $storeID; // dynamodb not be set up with a range key

    return $key;
}

function buildNotificationEventRequest( string $notification_event ) : array {
    $request = [];
    $request['live'] = true;
    $request['notificationItems'] = [];
    $request['notificationItems'][0]['NotificationRequestItem'] = json_decode($notification_event);

    return $request;
}

function sendNotificationToMesh(string $url, array $data, string $method = "post") {
    $http_client = new Client([
        'base_url' => $url
    ]);
    
    echo "sending adyen notification to mesh via " . $url . "\n";
    $response = $http_client->request($method, $url, [
        'headers' => [
            'Accept'     => 'application/json',
            'Content-Type'  => 'application/json'
        ], 
        'json' => $data
    ]);

    return $response;
}